from django.urls import path, include
from .views import *

urlpatterns = [
    path('',MainView, name='mainpage'),
    path('news-add/',NewsAddView, name='news-add'),
]
