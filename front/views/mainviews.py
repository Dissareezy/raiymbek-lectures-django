from django.shortcuts import render,redirect
from ..models import *
from datetime import datetime

def MainView(request):
    news = News.objects.all()
    context ={
        'news':news
    }
    return render(request,'front/index.html',context)

def NewsAddView(request):
    if request.method == 'POST':
        title = request.POST['title']
        text = request.POST['text']
        News.objects.create(title=title,text=text,date=datetime.now())
        return redirect('mainpage')
    context = {

    }
    return render(request,'front/news-add.html',context)